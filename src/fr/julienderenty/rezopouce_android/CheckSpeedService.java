package fr.julienderenty.rezopouce_android;

import java.util.Date;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

public class CheckSpeedService extends Service {
    Intent intent;


	@Override
    public int onStartCommand(Intent workIntent, int flags, int startId) {
        String dataString = workIntent.getDataString();
        
        LocationManager locationManager = (LocationManager) this .getSystemService(Context.LOCATION_SERVICE); 
        
        LocationListener locationListener = new LocationListener() { 
        	public void onLocationChanged(Location location) { 
        		//location.getLatitude(); 
        		Toast.makeText(getApplicationContext(), "Current speed:" + location.getSpeed(), Toast.LENGTH_SHORT).show(); 
            	intent = new Intent("IntentSpeed");	

    	       intent.putExtra("time", new Date());
    	        intent.putExtra("speed", location.getSpeed());
    	        sendBroadcast(intent);
    		} 
        	public void onStatusChanged(String provider, int status, Bundle extras) { } 
        	public void onProviderEnabled(String provider) { } 
        	public void onProviderDisabled(String provider) { } 
    	}; 
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		return startId;

    }

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
