package fr.julienderenty.rezopouce_android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
    	boolean start=true;

        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
            Log.d("Receiver","Boot complete");
        }else if(intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")){
            Log.d("Receiver","Power connected");
        }else if(intent.getAction().equals("android.bluetooth.BluetoothDevice.ACTION_ACL_CONNECTED")){
            Log.d("Receiver","Bluetooth connect");
        }else if(intent.getAction().equals("android.net.wifi.STATE_CHANGE")){
            Log.d("Receiver","Wifi connect");
        } else{
        	start=false;
        }
        
//        if(start){
//            // use this to start and trigger a service
//            Intent i= new Intent(context, CheckSpeedService.class);
//            // potentially add data to the intent
//            i.putExtra("KEY1", "Value to be used by the service");
//            context.startService(i); 
//        }

    }
}