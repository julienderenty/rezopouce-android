package fr.julienderenty.rezopouce_android;

import java.util.Date;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class MainActivity extends Activity {

    protected static final int requestCodeUID = 0;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
				startActivityForResult(intent, requestCodeUID);
				
			}
		});
        
        
        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
        	Button gcm = (Button) findViewById(R.id.button2);
        	gcm.setVisibility(View.VISIBLE);
        	gcm.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getApplicationContext(), GCMActivity.class);
					startActivity(intent);
				}
			});
        }
        
        
        findViewById(R.id.button3).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			     // use this to start and trigger a service
		        Intent i= new Intent(getApplicationContext(), CheckSpeedService.class);
		        // potentially add data to the intent
		        i.putExtra("KEY1", "Value to be used by the service");
		        getApplicationContext().startService(i); 				
			}
		});
        
        
        registerReceiver(
			new BroadcastReceiver() {
						
				@Override
				public void onReceive(Context arg0, Intent arg1) {
					Log.d("checkspeed", "location update at "+arg1.getStringExtra("time"));
					Toast.makeText(getApplicationContext(), "Status:" + arg1.getStringExtra("status"), Toast.LENGTH_SHORT).show(); 
					Toast.makeText(getApplicationContext(), "Current speed:" + arg1.getStringExtra("speed"), Toast.LENGTH_SHORT).show(); 
	
				}
			},  
			new IntentFilter("IntentSpeed"));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(requestCode, resultCode, data);
    	if(requestCode==requestCodeUID){
    		Toast.makeText(getApplicationContext(), "uid : "+data.getStringExtra("UID"), Toast.LENGTH_LONG).show();
    	}
    }
    
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("GCM", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    
}
